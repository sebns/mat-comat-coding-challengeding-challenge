# User events
This service listens for sensor events (GPS coordinates) and converts them into events that can be useful to users.

## Getting started
From the top directory (1 level up from this `README.md` file)
```sh
# Start all services
$ docker-compose up --build -d

# Check the race
$ sensible-browser http://127.0.0.1:8084/dashboard
```

## Links
- This service is using the python package [paho-mqtt](https://pypi.org/project/paho-mqtt).
- For unittest examples using a fake broker, see [paho.mqtt.python](https://github.com/eclipse/paho.mqtt.python).


## TO DO
- Unit tests.
- Calculate position on track (e.g. since begining of race).
