#!/usr/bin/env python3
"""MQTT example."""
import os
import re
import logging

from .listener import listener_loop

logging.basicConfig()


def get_arguments() -> dict:
    """Extract arguments from environment."""
    m = re.search(r'//(\S+):(\d+)', os.environ['MQTT_URL'])
    assert m, 'Invalid configuration for MQTT_URL'
    host, port = m.groups()

    return dict(
        broker_host=host,
        broker_port=int(port),
        topic_rx=os.environ['MQTT_TOPIC'])


kwargs = get_arguments()
listener_loop(**kwargs)
