#!/usr/bin/env python3
"""Module with backend Calculations."""
import os
from typing import Optional
from collections import namedtuple

from geopy.distance import distance
from geopy.point import Point

CarCoord = namedtuple('CarCoord', 'timestamp lat long')


def make_initial_state() -> dict:
    """Make an initial state."""
    return {'car_coord': {}, 'num_cars': os.environ.get('CAR_COUNT')}


def process_data(
        state: dict, timestamp: int, car_id: int,
        lat: float, long: float) -> Optional[float]:
    """Process new message data.
    :param state: state (preserved between calls)
    :param timestamp: epoch in ms
    :param car_id: car info relates to
    :param lat: Latitude
    :param long: longitude
    :return: speed in mph (or None for 1st data point)
    """
    car_old: CarCoord = state['car_coord'].get(car_id)
    if not car_old:
        speed_mph = None  # 1st data point (cannot determine speed)
    else:
        miles = distance(
            Point(latitude=car_old.lat, longitude=car_old.long),
            Point(latitude=lat, longitude=long)).miles
        hours = (timestamp - car_old.timestamp) / (1000.0 * 3600.0)
        speed_mph = miles / hours

    # Save state for next message
    state['car_coord'][car_id] = \
        CarCoord(timestamp=timestamp, lat=lat, long=long)
    return speed_mph
