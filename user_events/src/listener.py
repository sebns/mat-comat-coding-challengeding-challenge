#!/usr/bin/env python3
"""Listener converting sensor data to user-friendly data."""
import os
import json
import logging
from paho.mqtt.client import Client, MQTTMessage, MQTT_ERR_SUCCESS

from .backend import process_data, make_initial_state

logger = logging.getLogger(__name__)
logger.setLevel(os.environ.get('MQTT_LOG_LEVEL', 'WARNING'))

QOS = 0  # no need to resend old/delayed data


def listener_loop(broker_host: str, broker_port: int, topic_rx: str) -> None:
    """Main MQTT event loop.
    :param broker_host: host
    :param broker_port: port
    :param topic_rx: topic to subscribe to
    """
    userdata = make_initial_state()
    userdata['topic_coordinates'] = topic_rx
    client = Client(userdata=userdata)
    client.enable_logger()
    client.connect(host=broker_host, port=broker_port)
    try:
        # Subscribe to get input data
        client.on_message = _on_message
        result, mid = client.subscribe(topic_rx, qos=QOS)
        if result != MQTT_ERR_SUCCESS:
            raise Exception(f'Could not MQTT subscribe to {broker_host}!')

        client.loop_forever()
    finally:
        client.disconnect()


def _on_message(client: Client, userdata: dict, message: MQTTMessage) -> None:
    """Message handler.
    :param client: MQTT client
    :param userdata: any user data or state (preserved between messages)
    :param message: message received
    """
    if message.topic == userdata['topic_coordinates']:
        logger.debug(f'message received = {message.payload}')

        data = json.loads(message.payload.decode())
        if not isinstance(data, dict):
            logger.error(f'Received Unexpected payload: {message.payload}')
            return  # just ignore this message

        timestamp = data.get('timestamp')
        car_id = data.get('carIndex')
        location = data.get('location', {})
        speed = process_data(
            userdata, timestamp, car_id,
            location.get('lat'), location.get('long'))

        if speed is not None:
            # We have new speed information to send
            payload_data = dict(
                timestamp=timestamp, carIndex=car_id,
                type='SPEED', value=speed)
            msg_info = client.publish(
                topic="carStatus",
                payload=json.dumps(payload_data, indent=2),
                qos=QOS,
            )
            if msg_info.rc != MQTT_ERR_SUCCESS:
                logger.error(f'publish returned = {msg_info}')

        ## When we detect a car order change, we should also publish:
        # payload_data = dict(
        #     timestamp=timestamp,
        #     text='Car 2 races ahead of Car 4 in a dramatic overtake.')
        # client.publish_obj(
        #     topic="events",
        #     payload=json.dumps(payload_data, indent=2),
        #     qos=QOS,
        # )

        # Update state (for next message)
        client.user_data_set(userdata)
    else:
        logger.error(f'Received Unexpected topic: {message.topic}')
